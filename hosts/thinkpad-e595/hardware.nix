{
  config,
  lib,
  pkgs,
  modulesPath,
  ...
}:

with lib;
{
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  fileSystems."/" = {
    device = "/dev/disk/by-label/nixos";
    fsType = "ext4";
    options = [
      "noatime"
      "x-gvfs-hide"
    ];
  };

  fileSystems."/home" = {
    device = "/dev/disk/by-label/home";
    fsType = "ext4";
    neededForBoot = true;
    options = [
      "noatime"
      "x-gvfs-hide"
    ];
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-label/BOOT";
    fsType = "vfat";
    options = [ "x-gvfs-hide" ];
  };

  swapDevices = [ { device = "/dev/disk/by-label/swap"; } ];

  boot = {
    kernelPackages = pkgs.linuxPackages_latest;
    kernelModules = [ "bfq" ];
    kernelParams = [
      "pcie_aspm=off"
      "pci=pcie_bus_perf"
      "mitigations=off" # Significant performance boost, not multi-user.
    ];
    kernel.sysctl = {
      "net.ipv4.icmp_echo_ignore_broadcasts" = 1; # Refuse ICMP echo requests
    };
  };

  services.udev.extraRules = ''
    ACTION=="add|change", KERNEL=="nvme[0-9]*", ATTR{queue/scheduler}="none"
    ACTION=="add|change", KERNEL=="sd[a-z]|mmcblk[0-9]*", ATTR{queue/rotational}=="0", ATTR{queue/scheduler}="mq-deadline"
    ACTION=="add|change", KERNEL=="sd[a-z]", ATTR{queue/rotational}=="1", ATTR{queue/scheduler}="bfq"
  '';

  hardware.cpu.amd.updateMicrocode = true;
  powerManagement.cpuFreqGovernor = "performance";

  # Recommended, define logical cores manually
  nix.settings.max-jobs = mkDefault 4;

  # Manage device power-control:
  services.power-profiles-daemon.enable = true;

  # Finally, our beloved hardware module(s):
  modules.hardware = {
    plymouth.enable = true;
    pipewire.enable = true;
    bluetooth.enable = true;
    kmonad.deviceID = "/dev/input/by-path/platform-i8042-serio-0-event-kbd";
    pointer.enable = true;
    printer.enable = true;
    razer.enable = true;
  };

  services = {
    upower.enable = true;
    libinput.touchpad = {
      accelSpeed = "0.5";
      accelProfile = "adaptive";
    };
  };
}
