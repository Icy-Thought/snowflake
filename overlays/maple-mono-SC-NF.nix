_: prev: {
  maple-mono-SC-NF = prev.maple-mono-SC-NF.overrideAttrs (old: {
    version = "7.0-beta36";
    src = prev.fetchurl {
      url = "https://github.com/subframe7536/maple-font/releases/download/v7.0-beta36/MapleMono-NF-CN.zip";
      hash = "sha256-W5b4jcr6fGaAbasCXCswGMkG/SklCXUbfRcPvZfzsNo=";
    };
  });
}
