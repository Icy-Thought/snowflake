{
  options,
  config,
  lib,
  pkgs,
  ...
}:

with lib;
{
  options.modules.develop.markup = {
    latex.enable = mkEnableOption "bloated doc/math lang";
    typst.enable = mkEnableOption "modern LaTeX alt.";
  };

  config = mkMerge [
    (mkIf config.modules.develop.markup.latex.enable {
      user.packages = with pkgs; [
        texlab
        texlive.combined.scheme-full
      ];
    })

    (mkIf config.modules.develop.markup.typst.enable {
      user.packages = with pkgs; [
        typst
        tinymist
        typstyle
      ];
    })

    (mkIf config.modules.develop.xdg.enable {
      # TODO:
    })
  ];
}
